# UltimateBreadcrumbsView

## Introduction:
UltimaleBreadcrumbView is a customized Breadcrumbs with many added flexible features that gives you control over your content path that you can use to navigate easily through your application.
This contains dynamic list container fetaure by which you can add items dynamically.

## Usage Instructions:

Step 1:

Add the UltimateBreadcrumbsView to your layout:
    xml:
    Simple
     <com.abdshammout.UBV.UltimateBreadcrumbsView
                ohos:id="$+id:ultimateBreadcrumbsView"
                ohos:height="match_content"
                ohos:width="match_parent"
                ohos:top_margin="20">
     </com.abdshammout.UBV.UltimateBreadcrumbsView>

     Advance
     <com.abdshammout.UBV.UltimateBreadcrumbsView
                 ohos:id="$+id:ultimateBreadcrumbsView1"
                 ohos:height="match_content"
                 ohos:width="match_parent"
                 ohos:bottom_margin="32vp"
                 app:activePathItemBackground="$graphic:bg_two_corner_active"
                 app:activePathItemTextColor="#ffffffff"
                 app:backButtonBackground="$graphic:bg_two_corner"
                 app:backButtonIcon="$graphic:ic_back_1"
                 app:pathItemBackground="$graphic:bg_two_corner"
                 app:pathItemTextColor="#ff000000"
                 >
     </com.abdshammout.UBV.UltimateBreadcrumbsView>

Or init in code

     Java:
             ultimateBreadcrumbsView = (UltimateBreadcrumbsView) findComponentById(ResourceTable.Id_ultimateBreadcrumbsView);


              PathItemStyle pathItemStyle = new PathItemStyle();

              ShapeElement activePathItemBackgrouond = new ShapeElement();
              activePathItemBackgrouond.setShape(ShapeElement.RECTANGLE);
              activePathItemBackgrouond.setRgbColor(new RgbColor(255, 82, 144));
              activePathItemBackgrouond.setCornerRadiiArray(new float[]{20, 20, 0, 0, 20, 20, 0, 0});
              pathItemStyle.setActivePathItemBackgroundDrawable(activePathItemBackgrouond);

              ShapeElement pathItemBackgrouond = new ShapeElement();
              pathItemBackgrouond.setShape(ShapeElement.RECTANGLE);
              pathItemBackgrouond.setRgbColor(new RgbColor(175, 231, 238));
              pathItemBackgrouond.setCornerRadiiArray(new float[]{20, 20, 0, 0, 20, 20, 0, 0});
              pathItemStyle.setPathItemBackgroundDrawable(pathItemBackgrouond);
              ultimateBreadcrumbsView.setPathItemStyle(pathItemStyle);

              ultimateBreadcrumbsView.setBackButtonBackgroundRes(ResourceTable.Graphic_bg_two_corner);
              ultimateBreadcrumbsView.setBackButtonIconBitmap(ResUtil.getPixelMap(getContext(),ResourceTable.Media_ic_delete).get());

Step 2:
            ultimateBreadcrumbsView.setOnClickListenerBreadcrumbs(new OnClickListenerBreadcrumbs() {
            @Override
            public void onBackClick() {
                toast("onBackClick");
            }

            @Override
            public void onPathItemClick(int index, String title, int id) {
                String text = index + "  onPathItemClick = " + title;
                toast(text);
            }
        });

Step 3:
        Very important: The line must be added after the above instructions have been implemented (Must be the last instruction)
        (please do not forget this)

        //....
        //....
        //the codes mentioned above

        ultimateBreadcrumbsView.initUltimateBreadcrumbsView();

Step 4:
how to add/remove items
        //create PathItem
        //add just title
        PathItem pathItem = new PathItem("title");

        //Add item
        ultimateBreadcrumbsView.addToPath(pathItem);

        //Added in custom position
        ultimateBreadcrumbsView.addToPath(pathItem, 10);

        //Remove item
        ultimateBreadcrumbsView.back();

        //Remove all item to specific position
        ultimateBreadcrumbsView.backTo(3);


## Installation instructions:
Method 1:
    Generate the .har package through the library and add the .har package to the libs folder.
    Add the following code to the entry gradle:
        implementation fileTree  (dir: 'libs', include: ['*.jar', '*.har'])

Method 2:

Add following dependencies in entry build.gradle:**

    entry build.gradle:
    dependencies {
            implementation project(':library')
    }

Method 3:
    
For using UltimateBreadcrumbsView from a remote repository in separate application, add the below dependency in entry/build.gradle file.
 
 Modify entry build.gradle as below :
             
             
             ```gradle
             dependencies {
                    implementation 'io.openharmony.tpc.thirdlib:UltimateBreadcrumbsView:1.0.0'
             }
             ```   

## License

Copyright 2019 Abd Alrahman Shammout
   for the lights in our lives ☀

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
