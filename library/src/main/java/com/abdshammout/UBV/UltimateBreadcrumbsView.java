package com.abdshammout.UBV;

import com.abdshammout.UBV.model.PathItem;
import com.abdshammout.UBV.model.PathItemStyle;
import com.abdshammout.UBV.util.Constants;
import com.abdshammout.UBV.util.ResUtil;
import com.abdshammout.UBV.util.Utils;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.ElementScatter;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.agp.utils.LayoutAlignment;
import ohos.app.Context;
import ohos.media.image.PixelMap;

import java.util.Optional;

public class UltimateBreadcrumbsView extends DirectionalLayout {
    private BreadcrumbsListContainer breadcrumbsListContainer;
    private PathItemStyle pathItemStyle = new PathItemStyle();
    private OnClickListenerBreadcrumbs onClickListenerBreadcrumbs;
    private Context mContext;

    private Element activePathItem;
    private static final String activePathItemBackground = "activePathItemBackground";

    private Element pathItem;
    private static final String pathItemBackground = "pathItemBackground";

    private int activePathItemColor;
    private static final String activePathItemTextColor = "activePathItemTextColor";

    private int pathItemColor;
    private static final String pathItemTextColor = "pathItemTextColor";

    private Element backButtonBackgroundResItem;
    private static final String backButtonBackground = "backButtonBackground";


    private Element backButtonIconResItem;
    private static final String backButtonIcon = "backButtonIcon";


    public UltimateBreadcrumbsView(Context context) {
        super(context);
        mContext = context;
    }

    public UltimateBreadcrumbsView(Context context, AttrSet attrSet) {
        super(context, attrSet);
        mContext = context;
        initAttr(context, attrSet);
    }

    public UltimateBreadcrumbsView(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        mContext = context;
        initAttr(context, attrSet);
    }

    private void initAttr(Context context, AttrSet attrs){
        if (attrs != null) {
            activePathItem = attrs.getAttr(activePathItemBackground).isPresent() ?
                    attrs.getAttr(activePathItemBackground).get().getElement() : ElementScatter.getInstance(context).parse(ResourceTable.Graphic_bg_default_active_path);
            pathItem = attrs.getAttr(pathItemBackground).isPresent() ?
                    attrs.getAttr(pathItemBackground).get().getElement() : ElementScatter.getInstance(context).parse(ResourceTable.Graphic_bg_default_path);
            activePathItemColor = attrs.getAttr(activePathItemTextColor).isPresent() ?
                    attrs.getAttr(activePathItemTextColor).get().getColorValue().getValue(): 0xff000000;
            pathItemColor = attrs.getAttr(pathItemTextColor).isPresent() ?
                    attrs.getAttr(pathItemTextColor).get().getColorValue().getValue() : 0xffffffff;

            pathItemStyle.setActivePathItemTextColor(new Color(activePathItemColor));
            pathItemStyle.setPathItemTextColor(new Color(pathItemColor));

            pathItemStyle.setActivePathItemBackgroundDrawable(activePathItem);
            pathItemStyle.setPathItemBackgroundDrawable(pathItem);

            //back attributes
            if (attrs.getAttr(backButtonBackground).isPresent()){
                backButtonBackgroundResItem =  attrs.getAttr(backButtonBackground).get().getElement();
                setBackButtonBackgroundDrawable(backButtonBackgroundResItem);
            }

            if (attrs.getAttr(backButtonIcon).isPresent()){
                backButtonIconResItem = attrs.getAttr(backButtonIcon).get().getElement();
                setBackButtonIconDrawable(backButtonIconResItem);
            }
        }
    }



    private ListContainer CreateListContainer(){
        breadcrumbsListContainer = new BreadcrumbsListContainer(mContext);
        breadcrumbsListContainer.setLayoutDirection(LayoutDirection.LTR);
        LayoutConfig params = new LayoutConfig(ComponentContainer.LayoutConfig.MATCH_PARENT,
               ComponentContainer.LayoutConfig.MATCH_CONTENT);
        breadcrumbsListContainer.setOrientation(Component.HORIZONTAL);
        breadcrumbsListContainer.setLayoutConfig(params);
        breadcrumbsListContainer.setOnClickListenerBreadcrumbs(onClickListenerBreadcrumbs);
        breadcrumbsListContainer.setPathItemStyle(pathItemStyle);
        return breadcrumbsListContainer;
    }

    private Image createButtonBack(){
        Image buttonBack = new Image(mContext);
        setBackgroundBackButton(buttonBack);
        setIconBackButton(buttonBack);
        int margin = Utils.convertDpToPx(mContext,4);
        int padding = margin*2;
        LayoutConfig params= new LayoutConfig(LayoutConfig.MATCH_CONTENT, ComponentContainer.LayoutConfig.MATCH_CONTENT);
        params.setMargins(margin,margin,margin,margin);
        buttonBack.setLayoutConfig(params);
        buttonBack.setPadding(padding,padding,padding,padding);
        buttonBack.setClickedListener(new ClickedListener() {
            @Override
            public void onClick(Component component) {
                back();
                if (onClickListenerBreadcrumbs != null)
                    onClickListenerBreadcrumbs.onBackClick();
            }
        });
        return buttonBack;
    }

    //--------- public Method ------------
    public void initUltimateBreadcrumbsView(){
        setOrientation(HORIZONTAL);
        setAlignment(LayoutAlignment.VERTICAL_CENTER);
        addComponent(createButtonBack());
        addComponent(CreateListContainer());
    }

    public void setOnClickListenerBreadcrumbs(OnClickListenerBreadcrumbs onClickListenerBreadcrumbs) {
        this.onClickListenerBreadcrumbs = onClickListenerBreadcrumbs;
    }

    public void setPathItemStyle(PathItemStyle pathItemStyle){
        this.pathItemStyle = pathItemStyle;
    }

    public void addToPath(PathItem pathItem){
        if (breadcrumbsListContainer == null){
            return;
        }
        breadcrumbsListContainer.addToPath(pathItem);
    }

    public void addToPath(PathItem pathItem, int position){
        if (breadcrumbsListContainer == null){
            return;
        }
        breadcrumbsListContainer.addToPath(pathItem, position);
    }

    public void back(){
        if (breadcrumbsListContainer == null){
            return;
        }
        breadcrumbsListContainer.back();
    }

    public void backTo(int position){
        if (breadcrumbsListContainer == null){
            return;
        }
        breadcrumbsListContainer.backTo(position);
    }

    public int getItemCount(){
        if (breadcrumbsListContainer == null){
            return -1;
        }
        return breadcrumbsListContainer.getItemCount();
    }



    public void setActiveItemButtonBackground(Element element){
        PathItemStyle pathItemStyle = new PathItemStyle();
        pathItemStyle.setActivePathItemBackgroundDrawable(element);
    }

    //************** Button Back *****************
    private int backButtonBackgroundRes = ResourceTable.Graphic_bg_default_back_background;
    private RgbColor backButtonBackgroundColor;
    private Element backButtonBackgroundDrawable;
    private int backButtonBackgroundType = 1;

    public void setBackButtonBackgroundRes(int backButtonBackgroundRes) {
        this.backButtonBackgroundRes = backButtonBackgroundRes;
        backButtonBackgroundType = 1;
    }
    public void setBackButtonBackgroundColor(RgbColor backButtonBackgroundColor) {
        this.backButtonBackgroundColor = backButtonBackgroundColor;
        backButtonBackgroundType = 2;
    }
    public void setBackButtonBackgroundDrawable(Element backButtonBackgroundDrawable) {
        this.backButtonBackgroundDrawable = backButtonBackgroundDrawable;
        backButtonBackgroundType = 3;
    }
    private void setBackgroundBackButton(Image imageView){
        switch (backButtonBackgroundType){
            case Constants.TYPE_INT_RES:
                imageView.setBackground(ElementScatter.getInstance(imageView.getContext()).parse(backButtonBackgroundRes));
                break;
            case Constants.TYPE_INT_COLOR:
                ShapeElement shapeElement = new ShapeElement();
                shapeElement.setRgbColor(new RgbColor(backButtonBackgroundColor));
                imageView.setBackground(shapeElement);
                break;
            case Constants.TYPE_DRAWABLE:
                imageView.setBackground(backButtonBackgroundDrawable);
                break;
        }
    }


    private int backButtonIconRes = ResourceTable.Graphic_ic_back_black;
    private Element backButtonIconDrawable;
    private PixelMap backButtonIconBitmap;
    private int backButtonIconType = 1;

    public void setBackButtonIconRes(int backButtonIconRes) {
        this.backButtonIconRes = backButtonIconRes;
        backButtonIconType = Constants.TYPE_INT_RES;
    }
    public void setBackButtonIconDrawable(Element backButtonIconDrawable) {
        this.backButtonIconDrawable = backButtonIconDrawable;
        backButtonIconType = Constants.TYPE_DRAWABLE;
    }
    public void setBackButtonIconBitmap(PixelMap backButtonIconBitmap) {
        this.backButtonIconBitmap = backButtonIconBitmap;
        backButtonIconType = Constants.TYPE_BITMAP;
    }
    private void setIconBackButton(Image imageView){
        switch (backButtonIconType){
            case Constants.TYPE_INT_RES:
                imageView.setImageElement(ElementScatter.getInstance(imageView.getContext()).parse(backButtonIconRes));
                break;
            case Constants.TYPE_DRAWABLE:
                imageView.setImageElement(backButtonIconDrawable);
                break;
            case Constants.TYPE_BITMAP:
                imageView.setPixelMap(backButtonIconBitmap);
                break;
        }
    }
}
