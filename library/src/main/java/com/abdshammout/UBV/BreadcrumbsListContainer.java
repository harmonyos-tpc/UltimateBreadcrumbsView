package com.abdshammout.UBV;

import com.abdshammout.UBV.adapter.AdapterPath;
import com.abdshammout.UBV.model.PathItem;
import com.abdshammout.UBV.model.PathItemStyle;
import ohos.agp.components.AttrSet;
import ohos.agp.components.DirectionalLayoutManager;
import ohos.agp.components.ListContainer;
import ohos.app.Context;
import ohos.global.configuration.Configuration;

import java.util.ArrayList;

public class BreadcrumbsListContainer extends ListContainer implements OnClickPathItem {
    private AdapterPath adapterPath;
    private ArrayList<PathItem> pathItemsList;
    private OnClickListenerBreadcrumbs onClickListenerBreadcrumbs;
    private Context mContext;
    private boolean breadCrumbsClick;

    public BreadcrumbsListContainer(Context context) {
        super(context);
        mContext = context;
        initRecyclerViewPath();
    }

    public BreadcrumbsListContainer(Context context, AttrSet attrSet) {
        super(context, attrSet);
        initRecyclerViewPath();
    }

    public BreadcrumbsListContainer(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        initRecyclerViewPath();
    }

    public void setOnClickListenerBreadcrumbs(OnClickListenerBreadcrumbs onClickListenerBreadcrumbs) {
        this.onClickListenerBreadcrumbs = onClickListenerBreadcrumbs;
    }


    private void initRecyclerViewPath() {
        pathItemsList = new ArrayList<>();
        adapterPath = new AdapterPath(mContext, pathItemsList, this);
        DirectionalLayoutManager linearLayoutManager = new DirectionalLayoutManager();
        linearLayoutManager.setOrientation(HORIZONTAL);
        setLayoutManager(linearLayoutManager);

        setItemProvider(adapterPath);
    }

    public void setPathItemStyle(PathItemStyle pathItemStyle) {
        adapterPath.setPathItemStyle(pathItemStyle);
    }

    public void addToPath(PathItem pathItem) {
        if (pathItemsList == null) {
            return;
        }
        if (adapterPath == null) {
            return;
        }
        pathItemsList.add(pathItem);
        if (pathItemsList.size() == 1) {
            adapterPath.notifyDataChanged();
        } else {
            adapterPath.notifyDataSetItemInserted(adapterPath.getCount());
            adapterPath.notifyDataSetItemChanged(adapterPath.getCount() - 2);
        }
        if (mContext.getResourceManager().getConfiguration().direction == Configuration.DIRECTION_VERTICAL) {
            if (adapterPath.getCount() > 9) {
                scrollTo(adapterPath.getCount());
            } else {
                scrollTo(0);
            }
        } else {
            if (adapterPath.getCount() > 16) {
                scrollTo(adapterPath.getCount());
            } else {
                scrollTo(0);
            }
        }
    }


    public void addToPath(PathItem pathItem, int position) {
        if (pathItemsList == null) {
            return;
        }
        if (adapterPath == null) {
            return;
        }
        if (position > pathItemsList.size()) {
            return;
        }
        pathItemsList.add(position, pathItem);
        if (pathItemsList.size() == 1) {
            adapterPath.notifyDataChanged();
        } else {
            adapterPath.notifyDataSetItemInserted(position);
            adapterPath.notifyDataSetItemRangeChanged(position - 1, getItemCount() - 1);
        }
        if (mContext.getResourceManager().getConfiguration().direction == Configuration.DIRECTION_VERTICAL) {
            if (adapterPath.getCount() > 9) {
                scrollTo(adapterPath.getCount());
            } else {
                scrollTo(0);
            }
        } else {
            if (adapterPath.getCount() > 16) {
                scrollTo(adapterPath.getCount());
            } else {
                scrollTo(0);
            }
        }
    }

    public void back() {
        if (pathItemsList.isEmpty())
            return;
        adapterPath.notifyDataSetItemRemoved(pathItemsList.size() - 1);
        pathItemsList.remove(pathItemsList.size() - 1);
        adapterPath.notifyDataSetItemChanged(pathItemsList.size() - 1);
        if (!breadCrumbsClick) {
            if (mContext.getResourceManager().getConfiguration().direction == Configuration.DIRECTION_VERTICAL) {
                if (adapterPath.getCount() > 9) {
                    scrollTo(adapterPath.getCount());
                } else {
                    scrollTo(0);
                }
            } else {
                if (adapterPath.getCount() > 16) {
                    scrollTo(adapterPath.getCount());
                } else {
                    scrollTo(0);
                }
            }
            breadCrumbsClick = false;
        }
    }

    public void backTo(int position) {
        int levelBack = (adapterPath.getCount() - 1) - position;
        for (int i = 0; i < levelBack; i++)
            back();
    }

    public int getItemCount() {
        return adapterPath.getCount();
    }


    @Override
    public void onClick(int index, String title, int id) {
        breadCrumbsClick = true;
        backTo(index);
        if (onClickListenerBreadcrumbs != null) {
            onClickListenerBreadcrumbs.onPathItemClick(index, title, id);
        }
    }

    @Override
    public void onLongClick(int index, String title, int id) {
        if (onClickListenerBreadcrumbs != null)
            onClickListenerBreadcrumbs.onPathItemLongClick(index, title, id);
    }
}
