package com.abdshammout.UBV.model;


import com.abdshammout.UBV.ResourceTable;
import com.abdshammout.UBV.util.Constants;
import com.abdshammout.UBV.util.ResUtil;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.ElementScatter;
import ohos.agp.utils.Color;


public class PathItemStyle {

    //******** Path Item Background ********
    private int pathItemBackgroundResId = ResourceTable.Graphic_bg_default_path;
    private RgbColor pathItemBackgroundColor;

    private Element pathItemBackgroundDrawable;

    private int typeBackgroundPathItem = 1;

    //******** set ********
    public void setPathItemBackgroundResId(int ResId){
        pathItemBackgroundResId = ResId;
        typeBackgroundPathItem = Constants.TYPE_INT_RES;
    }

    public void setPathItemBackgroundColor(RgbColor color){
        pathItemBackgroundColor = color;
        typeBackgroundPathItem = Constants.TYPE_INT_COLOR;
    }

    public void setPathItemBackgroundDrawable(Element drawable){
        pathItemBackgroundDrawable = drawable;
        typeBackgroundPathItem = Constants.TYPE_DRAWABLE;
    }

    //******** get ********
    public int getPathItemBackgroundResId() {
        return pathItemBackgroundResId;
    }

    public RgbColor getPathItemBackgroundColor() {
        return pathItemBackgroundColor;
    }

    public Element getPathItemBackgroundDrawable() {
        return pathItemBackgroundDrawable;
    }

    public int getTypeBackgroundPathItem() {
        return typeBackgroundPathItem;
    }






    //******** Active Path Item Background ********
    private int ActivePathItemBackgroundResId = ResourceTable.Graphic_bg_default_active_path;

    private RgbColor ActivePathItemBackgroundColor;

    private Element ActivePathItemBackgroundDrawable;

    private int ActivePathItemBackgroundType = 1;

    //******** set ********

    public void setActivePathItemBackgroundResId(int ResId){
        ActivePathItemBackgroundResId = ResId;
        ActivePathItemBackgroundType = Constants.TYPE_INT_RES;
    }

    public void setActivePathItemBackgroundColor(RgbColor color){
        ActivePathItemBackgroundColor = color;
        ActivePathItemBackgroundType = Constants.TYPE_INT_COLOR;
    }

    public void setActivePathItemBackgroundDrawable(Element drawable){
        ActivePathItemBackgroundDrawable = drawable;
        ActivePathItemBackgroundType = Constants.TYPE_DRAWABLE;
    }

    //******** get ********
    public int getActivePathItemBackgroundResId() {
        return ActivePathItemBackgroundResId;
    }

    public RgbColor getActivePathItemBackgroundColor() {
        return ActivePathItemBackgroundColor;
    }

    public Element getActivePathItemBackgroundDrawable() {
        return ActivePathItemBackgroundDrawable;
    }

    public int getActivePathItemBackgroundType() {
        return ActivePathItemBackgroundType;
    }




    //******** path Item Text Color ********

    private Color pathItemTextColor = Color.BLACK;

    public void setPathItemTextColor(Color pathItemTextColor) {
        this.pathItemTextColor = pathItemTextColor;
    }

    public Color getPathItemTextColor() {
        return pathItemTextColor;
    }


    //******** Active path Item Text Color ********
    private Color ActivePathItemTextColor = Color.WHITE;

    public void setActivePathItemTextColor(Color activePathItemTextColor) {
        this.ActivePathItemTextColor = activePathItemTextColor;
    }

    public Color getActivePathItemTextColor() {
        return ActivePathItemTextColor;
    }



}
