package com.abdshammout.UBV.util;


import ohos.app.Context;

public class Utils {
    public static int convertDpToPx(Context context, int dps){
        return (int)(context.getResourceManager().getDeviceCapability().screenDensity / 160 * dps);
    }
}
