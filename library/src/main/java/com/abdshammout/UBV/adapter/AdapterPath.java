package com.abdshammout.UBV.adapter;


import com.abdshammout.UBV.OnClickPathItem;
import com.abdshammout.UBV.ResourceTable;
import com.abdshammout.UBV.model.PathItem;
import com.abdshammout.UBV.model.PathItemStyle;
import com.abdshammout.UBV.util.Constants;
import com.abdshammout.UBV.util.ResUtil;
import com.abdshammout.UBV.util.Utils;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.agp.components.element.ElementScatter;
import ohos.agp.components.element.ShapeElement;
import ohos.app.Context;

import java.util.ArrayList;

public class AdapterPath extends BaseItemProvider {

    private Context context;
    private ArrayList<PathItem> pathItemsList;
    private OnClickPathItem onClickPathItem;
    private PathItemStyle pathItemStyle;
    private DirectionalLayout.LayoutConfig itemPathParams;
    private DirectionalLayout.LayoutConfig activeItemPathParams;

    public AdapterPath(Context context, ArrayList<PathItem> pathItemsList,
                       OnClickPathItem onClickPathItem) {
        this.context = context;
        this.pathItemsList = pathItemsList;
        this.onClickPathItem = onClickPathItem;
        this.pathItemStyle = new PathItemStyle();
        initLayoutParams();
    }

    public void setPathItemStyle(PathItemStyle pathItemStyle) {
        this.pathItemStyle = pathItemStyle;
    }

    private void initLayoutParams() {
        int margin = Utils.convertDpToPx(context, 3);
        itemPathParams = new DirectionalLayout.LayoutConfig(DirectionalLayout.LayoutConfig.MATCH_CONTENT,
                DirectionalLayout.LayoutConfig.MATCH_CONTENT);
        itemPathParams.setMargins(margin, margin, margin, margin);

        activeItemPathParams = new DirectionalLayout.LayoutConfig(DirectionalLayout.LayoutConfig.MATCH_CONTENT,
                DirectionalLayout.LayoutConfig.MATCH_CONTENT);
        activeItemPathParams.setMargins(margin, margin, margin * 16, margin);
    }


    @Override
    public int getCount() {
        return pathItemsList.size();
    }

    @Override
    public Object getItem(int i) {
        return pathItemsList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public Component getComponent(int position, Component component, ComponentContainer componentContainer) {
        Component rootView = LayoutScatter.getInstance(context)
                .parse(ResourceTable.Layout_item_path, null, false);
        Text textComponent = (Text) rootView.findComponentById(ResourceTable.Id_pathItemTitle);
        textComponent.setText(pathItemsList.get(position).getTitle());

        if (position == getCount() - 1) {
            setActiveItemPathStyle(textComponent, position);
        } else {
            setItemPathStyle(textComponent, position);
        }

        textComponent.setClickedListener(component12 -> {
            PathItem pathItem = pathItemsList.get(position);
            onClickPathItem.onClick(position, pathItem.getTitle(), pathItem.getId());
        });

        textComponent.setLongClickedListener(component1 -> {
            PathItem pathItem = pathItemsList.get(position);
            onClickPathItem.onLongClick(position, pathItem.getTitle(), pathItem.getId());
        });

        return rootView;
    }

    private void setItemPathStyle(Text titlePathItem, int position) {
        PathItemStyle pathItemStyle;
        if (pathItemsList.get(position).getPathItemStyle() == null)
            pathItemStyle = this.pathItemStyle;
        else
            pathItemStyle = pathItemsList.get(position).getPathItemStyle();

        setItemPathBackground(titlePathItem, pathItemStyle);
        setItemPathTextColor(titlePathItem, pathItemStyle);
        titlePathItem.setLayoutConfig(itemPathParams);
    }

    private void setItemPathBackground(Text titlePathItem, PathItemStyle pathItemStyle) {
        switch (pathItemStyle.getTypeBackgroundPathItem()) {
            case Constants.TYPE_INT_RES:
                titlePathItem.setBackground(ElementScatter.getInstance(titlePathItem.getContext()).parse(pathItemStyle.getPathItemBackgroundResId()));
                break;
            case Constants.TYPE_INT_COLOR:
                ShapeElement shapeElement = new ShapeElement();
                shapeElement.setRgbColor(pathItemStyle.getPathItemBackgroundColor());
                titlePathItem.setBackground(shapeElement);
                break;
            case Constants.TYPE_DRAWABLE:
                titlePathItem.setBackground(pathItemStyle.getPathItemBackgroundDrawable());
                break;
        }
    }

    private void setItemPathTextColor(Text titlePathItem, PathItemStyle pathItemStyle) {
        titlePathItem.setTextColor(pathItemStyle.getPathItemTextColor());
    }

    private void setActiveItemPathStyle(Text titlePathItem, int position) {
        PathItemStyle pathItemStyle;
        if (pathItemsList.get(position).isUseStyleAlsoInActive())
            pathItemStyle = pathItemsList.get(position).getPathItemStyle();
        else
            pathItemStyle = this.pathItemStyle;

        setActiveItemPathBackground(titlePathItem, pathItemStyle);
        setActiveItemPathTextColor(titlePathItem, pathItemStyle);
        titlePathItem.setLayoutConfig(activeItemPathParams);
    }

    private void setActiveItemPathBackground(Text titlePathItem, PathItemStyle pathItemStyle) {
        switch (pathItemStyle.getActivePathItemBackgroundType()) {
            case Constants.TYPE_INT_RES:
                titlePathItem.setBackground(ElementScatter.getInstance(titlePathItem.getContext()).parse(pathItemStyle.getActivePathItemBackgroundResId()));
                break;
            case Constants.TYPE_INT_COLOR:
                ShapeElement shapeElement = new ShapeElement();
                shapeElement.setRgbColor(new RgbColor(pathItemStyle.getActivePathItemBackgroundColor()));
                titlePathItem.setBackground(shapeElement);
                break;
            case Constants.TYPE_DRAWABLE:
                titlePathItem.setBackground(pathItemStyle.getActivePathItemBackgroundDrawable());
                break;
        }
    }

    private void setActiveItemPathTextColor(Text titlePathItem, PathItemStyle pathItemStyle) {
        titlePathItem.setTextColor(pathItemStyle.getActivePathItemTextColor());
    }
}
