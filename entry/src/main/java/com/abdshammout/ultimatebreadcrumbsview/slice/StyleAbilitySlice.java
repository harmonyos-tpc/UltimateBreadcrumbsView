/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.abdshammout.ultimatebreadcrumbsview.slice;

import com.abdshammout.UBV.UltimateBreadcrumbsView;
import com.abdshammout.UBV.model.PathItem;
import com.abdshammout.UBV.model.PathItemStyle;
import com.abdshammout.ultimatebreadcrumbsview.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;

public class StyleAbilitySlice extends AbilitySlice {
    UltimateBreadcrumbsView ultimateBreadcrumbsView1;
    UltimateBreadcrumbsView ultimateBreadcrumbsView2;
    UltimateBreadcrumbsView ultimateBreadcrumbsView3;


    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_abilityslice_style);

        ultimateBreadcrumbsView1 = (UltimateBreadcrumbsView) findComponentById(ResourceTable.Id_ultimateBreadcrumbsView1);
        ultimateBreadcrumbsView2 = (UltimateBreadcrumbsView) findComponentById(ResourceTable.Id_ultimateBreadcrumbsView2);
        ultimateBreadcrumbsView3 = (UltimateBreadcrumbsView) findComponentById(ResourceTable.Id_ultimateBreadcrumbsView3);


        ultimateBreadcrumbsView1.initUltimateBreadcrumbsView();
        for (int i = 0; i < 5; i++) {
            ultimateBreadcrumbsView1.addToPath(new PathItem("title " + i));
        }


        ultimateBreadcrumbsView2.initUltimateBreadcrumbsView();
        for (int i = 0; i < 5; i++) {
            PathItem pathItem = new PathItem("title " + i);
            if (i == 2) {
                PathItemStyle pathItemStyle = new PathItemStyle();
                pathItemStyle.setPathItemBackgroundResId(ResourceTable.Graphic_bg_two_corner_sp);
                pathItem.setPathItemStyle(pathItemStyle, false);
            }
            ultimateBreadcrumbsView2.addToPath(pathItem);
        }


        ultimateBreadcrumbsView3.initUltimateBreadcrumbsView();
        for (int i = 0; i < 5; i++) {
            ultimateBreadcrumbsView3.addToPath(new PathItem("title " + i));
        }
    }
}
