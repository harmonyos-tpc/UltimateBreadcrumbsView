/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.abdshammout.ultimatebreadcrumbsview.slice;

import com.abdshammout.UBV.OnClickListenerBreadcrumbs;
import com.abdshammout.UBV.UltimateBreadcrumbsView;
import com.abdshammout.UBV.model.PathItem;
import com.abdshammout.UBV.model.PathItemStyle;
import com.abdshammout.UBV.util.ResUtil;
import com.abdshammout.ultimatebreadcrumbsview.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.window.dialog.ToastDialog;

public class SecondSampleAbilitySlice extends AbilitySlice {
    UltimateBreadcrumbsView ultimateBreadcrumbsView;

    Button addItem;
    Button addInPosition;
    Button removeItem;
    Button goToThirdSample, goToStyle;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_abilityslice_second_sample);

        ultimateBreadcrumbsView = (UltimateBreadcrumbsView) findComponentById(ResourceTable.Id_ultimateBreadcrumbsView);
        addItem = (Button) findComponentById(ResourceTable.Id_addItem);
        addInPosition = (Button) findComponentById(ResourceTable.Id_addInPosition);
        removeItem = (Button) findComponentById(ResourceTable.Id_removeItem);
        goToThirdSample = (Button) findComponentById(ResourceTable.Id_goToThirdSample);
        goToStyle = (Button) findComponentById(ResourceTable.Id_goToStyle);

        ultimateBreadcrumbsView.setOnClickListenerBreadcrumbs(new OnClickListenerBreadcrumbs() {
            @Override
            public void onBackClick() {
                toast("Back button pressed");
            }

            @Override
            public void onPathItemClick(int index, String title, int id) {
                String text = index + "  onPathItemClick = " + title;
                toast(text);
            }

            @Override
            public void onPathItemLongClick(int index, String title, int id) {
                String text = index + "  onPathItemLongClick = " + title;
                toast(text);
            }
        });


        initUBV();

        ultimateBreadcrumbsView.initUltimateBreadcrumbsView();

        ShapeElement background = new ShapeElement();
        background.setShape(ShapeElement.RECTANGLE);
        background.setRgbColor(new RgbColor(163, 194, 194));

        addItem.setBackground(background);
        addItem.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                ultimateBreadcrumbsView.addToPath(new PathItem("title"));
            }
        });

        addInPosition.setBackground(background);
        addInPosition.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                ultimateBreadcrumbsView.addToPath(new PathItem("title"), 5);
            }
        });

        removeItem.setBackground(background);
        removeItem.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                ultimateBreadcrumbsView.back();
            }
        });

        goToThirdSample.setBackground(background);
        goToThirdSample.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                ThirdSampleAbilitySlice thirdSampleAbilitySliceabilitySlice = new ThirdSampleAbilitySlice();
                Intent intent2 = new Intent();
                present(thirdSampleAbilitySliceabilitySlice, intent2);
            }
        });

        goToStyle.setBackground(background);
        goToStyle.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                StyleAbilitySlice styleAbilitySlice = new StyleAbilitySlice();
                Intent intent3 = new Intent();
                present(styleAbilitySlice, intent3);
            }
        });
    }

    public void toast(String text) {
        ToastDialog toastDialog = new ToastDialog(this);
        toastDialog.setText(text).setDuration(1000).show();
    }

    private void initUBV() {
        PathItemStyle pathItemStyle = new PathItemStyle();

        ShapeElement activePathItemBackgrouond = new ShapeElement();
        activePathItemBackgrouond.setShape(ShapeElement.RECTANGLE);
        activePathItemBackgrouond.setRgbColor(new RgbColor(255, 82, 144));
        activePathItemBackgrouond.setCornerRadiiArray(new float[]{20, 20, 0, 0, 20, 20, 0, 0});
        pathItemStyle.setActivePathItemBackgroundDrawable(activePathItemBackgrouond);

        ShapeElement pathItemBackgrouond = new ShapeElement();
        pathItemBackgrouond.setShape(ShapeElement.RECTANGLE);
        pathItemBackgrouond.setRgbColor(new RgbColor(175, 231, 238));
        pathItemBackgrouond.setCornerRadiiArray(new float[]{20, 20, 0, 0, 20, 20, 0, 0});
        pathItemStyle.setPathItemBackgroundDrawable(pathItemBackgrouond);
        ultimateBreadcrumbsView.setPathItemStyle(pathItemStyle);

        ultimateBreadcrumbsView.setBackButtonBackgroundRes(ResourceTable.Graphic_bg_two_corner);
        ultimateBreadcrumbsView.setBackButtonIconBitmap(ResUtil.getPixelMap(getContext(), ResourceTable.Media_ic_delete).get());

    }
}
